package hello;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.impl.util.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.core.GraphDatabase;

import java.io.File;

@Configuration
@EnableNeo4jRepositories(basePackages = "hello")
public class Application extends Neo4jConfiguration implements CommandLineRunner {

    public Application() {
        setBasePackage("hello");
    }

    @Bean
    GraphDatabaseService graphDatabaseService() {
        return new GraphDatabaseFactory().newEmbeddedDatabase("accessingdataneo4j.db");
    }

    @Autowired
    PersonRepository personRepository;

    @Autowired
    GraphDatabase graphDatabase;

    public void run(String... args) throws Exception {
        Person greg = new Person("Greg","Boy");
        Person roy = new Person("Roy","Cliff");
        Person craig = new Person("Craig","Danial");

        System.out.println("Before linking up with Neo4j...");
        for (Person person : new Person[]{greg, roy, craig}) {
            System.out.println(person);
        }

        Transaction tx = graphDatabase.beginTx();
        try {
            personRepository.save(greg);
            personRepository.save(roy);
            personRepository.save(craig);

            greg = personRepository.findByName(greg.getName());
            greg.worksWith(roy);
            greg.worksWith(craig);
            greg.isFriendOf(roy);
            personRepository.save(greg);

            roy = personRepository.findByName(roy.getName());
            roy.worksWith(craig);
            roy.isFriendOf(craig);
            // We already know that roy works with greg
            personRepository.save(roy);

            // We already know craig works with roy and greg

            System.out.println("Lookup each person by name...");
            for (String name: new String[]{greg.getName(), roy.getName(), craig.getName()}) {
                System.out.println(personRepository.findByName(name));
            }

            System.out.println("Looking up who works with Greg...");
            for (Person person : personRepository.findByTeammatesName("Greg")) {
                System.out.println(person.getName() + " works with Greg.");
            }

            System.out.println("\nLooking up who is a friend of Greg...");
            for (Person person : personRepository.findByFriendsName("Greg")) {
                System.out.println(person.getName() + " is a friend of Greg.");
            }

            System.out.println("\n testing idcheck:\n");
            System.out.println(personRepository.findOne(greg.getId()));

            System.out.println("\n testing FriendsFriends of Greg:\n(Greg->[FRIEND]->Roy->[FRIEND]->Craig)");
            for(Person friendOfFriend:personRepository.findFriendsFriend(personRepository.findOne(greg.getId()))){
                for (Person person : personRepository.findByFriendsName("Roy")){
                    if(friendOfFriend.getName().equals(person.getName()))
                    System.out.println("\t-" + friendOfFriend.getName() + " is the Friend of the Friend of Greg\n");
                    else
                        System.out.println("\t-" +person.getName()+" Roys Friend Greg exists because Greg->[FRIEND]->Roy\n\t-and Direction=both\n");
                }

            }

            tx.success();
        } finally {
            tx.close();
        }

    }

    public static void main(String[] args) throws Exception {
        FileUtils.deleteRecursively(new File("accessingdataneo4j.db"));

        SpringApplication.run(Application.class, args);
    }

}
