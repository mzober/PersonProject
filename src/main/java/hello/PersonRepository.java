package hello;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.Set;

public interface PersonRepository extends GraphRepository<Person> {

    Person findByName(String name);

    Iterable<Person> findByTeammatesName(String name);

    Iterable<Person> findByFriendsName(String name);

    @Override
    Person findOne(Long aLong);

    @Query(
            "start person=node({0}) " +
                    "match person-[:FRIEND]->(friend)-[:FRIEND]->(friendsFriend) " +
                    "return friendsFriend")
    Set<Person> findFriendsFriend(Person person);
}
